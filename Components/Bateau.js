// Components/Bateau.js

//Vue2X.png

import React from 'react'
import { Text, StyleSheet, View, Image, ImageBackground } from 'react-native'

class Bateau extends React.Component {
    render(){
		const nameBateau = this.props.navigation.state.params.nameBateau    
		const uriBateau = this.props.navigation.state.params.uriBateau 
		const desc = this.props.navigation.state.params.desc                                                      
		return (
			<ImageBackground source={require('../assets/splash.png')} style={styles.background}>
				<View style={styles.main_container}>
					<Text style={styles.text}>{nameBateau}</Text>
					<Image style={styles.image} source={uriBateau}></Image>
		   		 </View>
				<View style={styles.container2}>
						<Text style={styles.desc}>{desc}</Text>
				</View>
			</ImageBackground>  
		 )
	}
}

const styles = StyleSheet.create({
	main_container: {
	  flex: 1,
	  height: 2,
	  margin:10,
	  alignItems: 'center',
	  
	},
  
	container2: {
	  height: 200,
	},
	text:{
	  fontWeight: 'bold',
	  fontSize: 30,
	  flex: 1,
	  flexWrap: 'wrap',
	  paddingRight: 5,
	  textAlign: 'center',
	  margin :10,
	}, 
	desc:{
	  textAlign: 'center',
	  margin :8,
	  fontSize: 15,
	},
	
  image: {
	  width: 340,
	  height:280,
	  marginBottom:50,
	  backgroundColor: 'gray',
	  flexWrap: 'wrap',
	  justifyContent: 'center',
	  alignItems: 'center',
  },
	background: {
	  width: '100%',
	  height: '100%',
	  flex: 1,
	  //resizeMode: "cover",
	  //justifyContent: "center"
	}
  
  })
export default Bateau
