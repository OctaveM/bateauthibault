

//Icons
//Bateau: 
exports.aquilon_icon = require('../assets/Images/Bateaux/aquilon_icon.png');
exports.deLaBrise_icon = require('../assets/Images/Bateaux/deLaBrise_icon.png');
exports.saphir_icon = require('../assets/Images/Bateaux/saphir_icon.png'); 
exports.gastMicher_icon = require('../assets/Images/Bateaux/gastMicher_icon.png'); 

//Recette 
exports.barRecette_icon = require('../assets/Images/Recette/barRecette_icon.png');
exports.homardRecette_icon = require('../assets/Images/Recette/homardRecette_icon.png');
exports.saintJacques_icon = require('../assets/Images/Recette/saintJacques_icon.png'); 

//Restaurant 

exports.bistrotLandais_icon = require('../assets/Images/Restaurants/bistrotLandais_icon.png');
exports.desGascons_icon = require('../assets/Images/Restaurants/desGascons_icon.png');
exports.duSommelier_icon = require('../assets/Images/Restaurants/duSommelier_icon.png');
exports.fousDeLIle_icon = require('../assets/Images/Restaurants/fousDeLIle_icon.png');
exports.villa9Trois_icon = require('../assets/Images/Restaurants/villa9Trois_icon.png');

//Image
//Bateau:
exports.aquilon = require('../assets/Images/Bateaux/aquilon.png');
exports.deLaBrise = require('../assets/Images/Bateaux/deLaBrise.png');
exports.saphir = require('../assets/Images/Bateaux/saphir.png'); 
exports.gastMicher = require('../assets/Images/Bateaux/gastMicher.png'); 

exports.barRecette = require('../assets/Images/Recette/barRecette.png');
exports.homardRecette = require('../assets/Images/Recette/homardRecette.png');
exports.saintJacques = require('../assets/Images/Recette/saintJacques.png');

//Restaurant
exports.bistrotLandais = require('../assets/Images/Restaurants/bistrotLandais.png');
exports.desGascons = require('../assets/Images/Restaurants/desGascons.png');
exports.duSommelier = require('../assets/Images/Restaurants/duSommelier.png');
exports.fousDeLIle = require('../assets/Images/Restaurants/fousDeLIle.png');
exports.villa9Trois = require('../assets/Images/Restaurants/villa9Trois.png');
