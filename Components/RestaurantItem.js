// Components/RestaurantItem.js

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList , Button,Image, TouchableOpacity } from 'react-native'
import Icons from './Icons.js'

class RestaurantItem extends React.Component {
  render() {
    const { restaurant, displayDetailForRestaurant }  = this.props
  
    return (
      <TouchableOpacity   style={styles.main_container}  onPress={() => displayDetailForRestaurant(restaurant.name,Icons[restaurant.uri],restaurant.desc,restaurant.adresse)}>
       <Image style={styles.image} source = {Icons[restaurant.icon]} />
        <View style={styles.content_container}>
          <View style={styles.header_container}>
          	<Text style={styles.title_text}>{restaurant.name}</Text>   
          </View>
        </View>
      </TouchableOpacity>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 180,
    margin : 10,
    flexDirection: 'row',
    backgroundColor: 'rgba(120, 120, 120, 0.8)',
    borderColor : "black",
    borderWidth : 1
  },

  image: {
    width: 150,
    height: 150,
    margin: 10,
    backgroundColor: 'gray',
    borderRadius: 150/ 2
  },
  content_container: {
    flex: 10,
    margin: 5
  },
  header_container: {
    flex: 3,
    flexDirection: 'row'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5,
    textAlignVertical : 'center'
  }
})


export default RestaurantItem
