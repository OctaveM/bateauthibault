// Components/SelectionTypeProduit.js

//Vue5.png

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList } from 'react-native'
import typesProduits from '../assets/StaticData/typeProduitsData.js'
import TypeItem from './TypeItem'


class SelectionTypeProduit extends React.Component {


    constructor(props) {
      super(props)
      this.state = {_basket : []}
      this._addToBasket.bind(this)
      this._getBasket.bind(this)
      this._setQuantity.bind(this)
    }
  
    _getBasket = () => {
      return this.state._basket;
    }
    _addToBasket = ( item ) => {
      //console.log(item.id)
      if(this.state._basket.find(i=>i.id==item.id)==null)
        this.setState({_basket : this.state._basket.concat([{id: item.id, name: item.name, price : item.price, quantity : 1}])}, () => console.log (this.state._basket))
    }
    _setQuantity = (id, newQuantity) => {
      var temp = this.state._basket
      //si la nouvelle quantité est 0, on supprime l'objet du tableau
      if (newQuantity<1) {
        temp.splice(temp.findIndex(e=> e.id == id),1)
      }
      else {
        temp[temp.findIndex(e=> e.id == id)].quantity = newQuantity
      }
      this.setState({_basket : temp})
    }

    _displayDetailForType = ( nameType, varConstraint, valueConstraint) => {
	   this.props.navigation.navigate("Produits",{ nameType: nameType, varConstraint: varConstraint, valueConstraint: valueConstraint , basketAdd : this._addToBasket, basketGet : this._getBasket, basketQuantity : this._setQuantity} )
    }
    render() {
	
        return (
  	      <View style={styles.container}>
	          <ImageBackground source={require('../assets/splash.png')} style={styles.image}>
              <Text style={styles.text}>Choissisez vos produits </Text>
              <FlatList
                data={typesProduits}
                keyExtractor={(item) =>item.id.toString()}
                renderItem={({item}) =><TypeItem type={item} displayDetailForType = {this._displayDetailForType}/>}
                style ={styles.liste}
              />
            </ImageBackground>  
          </View>

        )

    }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
    //backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    flex: 1,
    //resizeMode: "cover",
    //justifyContent: "center"
  },
  text:{
    fontWeight: 'bold',
      fontSize: 20,
      flex: 0.1,
      flexWrap: 'wrap',
    paddingRight: 5,
    textAlign: 'center'
    },
  liste:{
      flex: 1
    }
});

export default SelectionTypeProduit
