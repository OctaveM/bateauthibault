// Components/Restaurants.js

//Vue3.png

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList } from 'react-native'
import restaurant from '../assets/StaticData/restaurantData.js'
import RestaurantItem from './RestaurantItem'


class Restaurants extends React.Component {

    _displayDetailForRestaurant = ( nameRestaurant,uriRestaurant,desc,adresse) => {
        this.props.navigation.navigate("Restaurant",{ nameRestaurant:nameRestaurant, uriRestaurant: uriRestaurant, desc:desc, adresse:adresse} )
      }
      render() {
      
          return (
              <View style={styles.container}>
                <ImageBackground source={require('../assets/splash.png')} style={styles.image}>
                <Text style={styles.text}>La liste des Restaurant</Text>
                <FlatList
                  data={restaurant}
                  keyExtractor={(item) =>item.id.toString()}
                  renderItem={({item}) =><RestaurantItem restaurant={item} displayDetailForRestaurant = {this._displayDetailForRestaurant}/>}
                  style ={styles.liste}
                />
              </ImageBackground>  
            </View>
  
          )
  
      }

}


const styles = StyleSheet.create({
  container: {
    flex: 1
    //backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    flex: 1,
    //resizeMode: "cover",
    //justifyContent: "center"
  },
  text:{
    fontWeight: 'bold',
      fontSize: 30,
      flex: 0.1,
      flexWrap: 'wrap',
    paddingRight: 5,
    textAlign: 'center'
    },
  liste:{
      flex: 1
    }
});


export default Restaurants
