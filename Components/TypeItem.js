// Components/TypeItemItem.js

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList , Button,Image, TouchableOpacity } from 'react-native'
import Icons from './Icons.js'

class TypeItem extends React.Component {
  render() {
    const { type, displayDetailForType }  = this.props
  
    return (
      <TouchableOpacity   style={styles.main_container}  onPress={() => displayDetailForType(type.name, type.var_check, type.value_check)}>
       <Image style={styles.image} source = {require('../assets/poulpe.png')} />
        <View style={styles.content_container}>
          <View style={styles.header_container}>
          	<Text style={styles.title_text}>{type.name}</Text>   
          </View>
        </View>
      </TouchableOpacity>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 120,
    margin : 10,
    flexDirection: 'row',
    backgroundColor: 'rgba(120, 120, 120, 0.8)',
    borderColor : "black",
    borderWidth : 1
  },

  image: {
    marginTop : 10,
    width: 100,
    height: 100,
    marginLeft: 10,
    marginRight: 10,
  },
  content_container: {
    flex: 10,
    margin: 5
  },
  header_container: {
    flex: 3,
    flexDirection: 'row'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5,
    textAlignVertical : 'center'
  }
})


export default TypeItem
