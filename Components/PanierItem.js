// Components/PanierItem.js


import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList , Button,Image, TouchableOpacity } from 'react-native'
import Icons from './Icons.js'
import Dialog from 'react-native-dialog'

class PanierItem extends React.Component {
  constructor(props){
    super(props)
    this.state ={visible:false}
    this._switchVisibility.bind(this)
  }

  _switchVisibility(){
    this.setState({visible : !this.state.visible})
  }

  render() {
    const { item , setQuantity}  = this.props
  
    return (
      <TouchableOpacity   style={styles.main_container}  onPress={()=>this._switchVisibility()}>
            <Image style={styles.image} source = {require('../assets/poulpe.png')} />
          	<Text style={styles.text}>{item.name} {item.quantity}*{item.price}</Text>
            <Dialog.Container visible={this.state.visible}>
              <Dialog.Title>Choississez la quantité</Dialog.Title>
              <View style={{flexDirection: 'row'}}>
              <Dialog.Button label="0" onPress={()=>{setQuantity(item.id,0);this._switchVisibility()}} />
              <Dialog.Button label="1" onPress={()=>{setQuantity(item.id,1);this._switchVisibility()}} />
              <Dialog.Button label="2" onPress={()=>{setQuantity(item.id,2);this._switchVisibility()}} />
              <Dialog.Button label="3" onPress={()=>{setQuantity(item.id,3);this._switchVisibility()}} />
              <Dialog.Button label="4" onPress={()=>{setQuantity(item.id,4);this._switchVisibility()}} />
              <Dialog.Button label="5" onPress={()=>{setQuantity(item.id,5);this._switchVisibility()}} />
              </View>
              <View style={{flexDirection: 'row'}}>
              <Dialog.Button label="Ajouter 1" onPress={()=>{setQuantity(item.id,item.quantity+1);this._switchVisibility()}} />
              <Dialog.Button label="Enlever 1" onPress={()=>{setQuantity(item.id,item.quantity-1);this._switchVisibility()}} />
              <Dialog.Button label="Annuler" onPress={()=>{this._switchVisibility()}} />
              </View>
            </Dialog.Container>
      </TouchableOpacity>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 40,
    margin : 5,
    flexDirection: 'row',
    backgroundColor: 'rgba(120, 120, 120, 0.8)'
  },

  image: {
        width: 40,
    height: 40,
    marginLeft: 10,
    marginRight: 10
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 25,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  }
})


export default PanierItem