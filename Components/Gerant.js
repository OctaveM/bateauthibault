// Components/Gerant.js

//Vue1.png

import React from 'react'
import { Text, ImageBackground, StyleSheet, View,Image} from 'react-native'

class Gerant extends React.Component {
  render() {
    return (
      <ImageBackground source={require('../assets/splash.png')} style={styles.image}>
      <View style={styles.container}>
          <View> 
			    <Text style = {styles.intitule}> Le bateau de Thibault </Text>
          <Image style = {styles.photo} source = {require('../assets/Images/Contact/TIG.png')}/>
            <Text style = {styles.text}> 06.63.99.99.78 </Text>
            <Text style = {styles.text}> lebateaudethibault@gmail.com </Text>
            <Text style = {styles.text}> www.facebook.com/lebateaudethibault </Text>
          </View>
          <View style={styles.container_desc}>
            <Text style = {styles.text}>Qu'il est chaud le soleil </Text>
            <Text style = {styles.text}>Quand nous sommes en vacances</Text>
            <Text style = {styles.text}>Y a d' la joie, des hirondelles</Text>
            <Text style = {styles.text}>C'est le sud de la France</Text>
            <Text style = {styles.text}>Papa bricole au garage</Text>
            <Text style = {styles.text}>Maman lit dans la chaise longue</Text>
            <Text style = {styles.text}>Dans ce joli paysage</Text>
            <Text style = {styles.text}>Moi, je me balade en tongs</Text>
            <Text style = {styles.text}>Que de bonheur!</Text>
            <Text style = {styles.text}>Que de bonheur!</Text>
            </View>
       </View>
       </ImageBackground>
            // Ici on rend à l'écran les éléments graphiques de notre component custom Search

        )

    }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
   margin:10,
  },
  image: {
    width: '100%',
    height: '100%',
    flex: 1,
    //resizeMode: "cover",
   
  },
  intitule: {
    fontSize: 30,
    textAlign: 'center',
   
    
  },
  text : {
    textAlign: 'center',
    fontSize: 16,
  },
  photo : {
  marginLeft :'32%',
	backgroundColor: 'gray',
  flexWrap: 'wrap',
  alignItems: 'center',
  justifyContent: "center",
  },
  container_desc: {
    margin:20,
  },

});

export default Gerant
