// Components/BateauItem.js

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList , Button,Image, TouchableOpacity } from 'react-native'
import Icons from './Icons.js'

class BateauItem extends React.Component {
  render() {
    const { bateau, displayDetailForBateau }  = this.props
  
    return (
      <TouchableOpacity   style={styles.main_container}  onPress={() => displayDetailForBateau(bateau.name,Icons[bateau.uri],bateau.desc)}>
       <Image style={styles.image} source = {Icons[bateau.icon]} />
        <View style={styles.content_container}>
          <View style={styles.header_container}>
          	<Text style={styles.title_text}>{bateau.name}</Text>   
          </View>
        </View>
      </TouchableOpacity>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 180,
    margin : 10,
    flexDirection: 'row',
    backgroundColor: 'rgba(120, 120, 120, 0.8)',
    borderColor : "black",
    borderWidth : 1
  },

  image: {
    width: 150,
    height: 150,
    margin: 5,
    backgroundColor: 'gray',
    borderRadius: 150/ 2,
  },
  content_container: {
    flex: 10,
    margin: 4
  },
  header_container: {
    flex: 4,
    flexDirection: 'row'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 25,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5,
    textAlignVertical : 'center'
  }
})


export default BateauItem
