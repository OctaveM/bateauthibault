// Components/Produits.js

//Vue2X.png

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList, Button, TouchableOpacity, Image } from 'react-native'
import ProduitItem from './ProduitItem'

    
//http://51.255.166.155:1352/tig/products/?format=json
class Produits extends React.Component {

    _loadProduits(varCons, valCons) {
      if(!this.state.produits.length) {
        fetch('http://51.255.166.155:1352/tig/products/?format=json').then(response => response.json()).catch(error => {alert("Votre requête est actuellement indisponible, veuillez réessayer ultérieurement")})
        .then(data => {
          this.setState({produits : data.filter(x => x.[varCons] === valCons)})
          this.setState({produitsLength : produits.length})
          this.forceUpdate()})
        
      }

    }

    _handleLoadMore = () =>{
      if (this.state.produitsCurrentBatch*this.state.batch<this.state.produitsLength){
        this.setState({produitsCurrentBatch : this.state.produitsCurrentBatch+1})
      }
    }
    _displayPanier = () => {
      console.log(this.props.navigation.state.params.basketGet())
     this.props.navigation.navigate("Panier",{ items : this.props.navigation.state.params.basketGet(), basketQuantity: this.props.navigation.state.params.basketQuantity} )
    }

    constructor(props) {
      super(props);
      this.state = {
        produits : [],
        batch : 10,
        produitsLength : 0,
        produitsCurrentBatch : 0
      }
    	/**/
    }
    render() {
      const varConstraint = this.props.navigation.state.params.varConstraint   
      const valueConstraint = this.props.navigation.state.params.valueConstraint
      this._loadProduits(varConstraint, valueConstraint)
        return (
  	      <View style={styles.container}>
  	      <ImageBackground source={require('../assets/splash.png')} style={styles.image}>
              <Text style={styles.text}> La liste des Produits </Text>
              <TouchableOpacity   style={styles.main_container}  onPress={() => this._displayPanier()}>
                <Image style={styles.imagePanier} source = {require('../assets/basket.png')} />
                <Text style={styles.title_text}>Panier</Text>
              </TouchableOpacity>
              <FlatList
                data={this.state.produits.slice(0, this.state.produitsCurrentBatch*this.state.batch+this.state.batch)}
                keyExtractor={(item) =>item.id.toString()}
                renderItem={({item}) =><ProduitItem produit={item} basketAdd ={this.props.navigation.state.params.basketAdd}/>}
                style ={styles.liste}
                onEndReached={this._handleLoadMore}
                onEndReachedThreshold={0.5}
                initialNumToRender={10}
              />
              </ImageBackground>  
          </View>

        )

    }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
    //backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    flex: 1,
    //resizeMode: "cover",
    //justifyContent: "center"
  },
  imagePanier: {
    width: 40,
    height: 40,
    //resizeMode: "cover",
    //justifyContent: "center"
  },
  text:{
    fontWeight: 'bold',
      fontSize: 20,
      flex: 0.1,
      flexWrap: 'wrap',
    paddingRight: 5,
    textAlign: 'center'
    },
  liste:{
      flex: 1
    }
});

export default Produits
