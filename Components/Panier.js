// Components/Panier.js

//Vue2X.png

import React from 'react'
import { Text, StyleSheet, View, Image, ImageBackground, FlatList, TouchableOpacity, Modal, TouchableHighlight } from 'react-native'
import Dialog from 'react-native-dialog'
import PanierItem from './PanierItem'

import restaurant from '../assets/StaticData/restaurantData.js'
import moment from 'moment';
import 'moment/locale/fr'

class Panier extends React.Component {

	constructor(props){
		super(props);
		console.log("panier")
		this.state = {items : this.props.navigation.state.params.items,
					  total : 0,//Toutes les quantités à 1 à ce moment...
					  livraison : restaurant[0],
					  visibleConfirmation : false,
					  modalVisible : false}
		this._setQuantity.bind(this)
	}

	_setQuantity = (id, newQuantity) => {
		this.props.navigation.state.params.basketQuantity(id, newQuantity)
		this.setState({total : this.state.items.reduce((acc, val) => acc+val.price*val.quantity,0)})
	}

	_switchVisibility = () => {
		this.setState({visibleConfirmation : !this.state.visibleConfirmation})
	}
	_switchModalVisibility = () => {
		this.setState({modalVisible : !this.state.modalVisible})
	}

	_dateLivraison() {
		moment.locale('fr')
		var today = moment().isoWeekday()
		switch(today){
			case 1: today = moment().isoWeekday(3); break;
			case 2: today = moment().isoWeekday(3); break;
			case 3: today = moment().isoWeekday(6); break;
			case 4: today = moment().isoWeekday(6); break;
			case 5: today = moment().isoWeekday(6); break;
			case 2: today = moment().isoWeekday(6); break;
			case 6: today = moment().isoWeekday(7); break;
			case 7: today = moment().add(1, 'weeks').isoWeekday(3); break;
		}
		today.locale('fr')
		return today.format('dddd DD/MM')
	}
	componentDidMount(){
		this.setState({total : this.state.items.reduce((acc, val) => acc+val.price,0)})
	}

    render(){                                          
		return (
			<ImageBackground source={require('../assets/splash.png')} style={styles.background}>
			<View style={styles.main_container}>
				<Modal 	animationType="slide"
        				transparent={true}
        				visible={this.state.modalVisible}>
        			<View style={styles.modal}>
        				<Text>Choississez le point de relais :</Text>
						{restaurant.map((x) => {return (<TouchableHighlight onPress={() => {this.setState({livraison : x}); this._switchModalVisibility()}}>
															<Text>{x.name}</Text>
														</TouchableHighlight>)})}
		            </View>
				</Modal>

				<Dialog.Container visible={this.state.visibleConfirmation}>
	              <Dialog.Title>Envoyer votre commande ?</Dialog.Title>
	              <Dialog.Description>Envoyer votre commande de {this.state.total}€ à Thibault ?</Dialog.Description>
	              <Dialog.Button label="Non" onPress={()=>{this._switchVisibility()}} />
	              <Dialog.Button label="Oui" onPress={()=>{this._switchVisibility(); this.props.navigation.popToTop()}} />
	            </Dialog.Container>
				<View style={styles.header_container}>
					<Text style={styles.text}>Modifiez la quantité en tapant sur chaque produit</Text>
				</View>
				<View style={styles.items_container}>
	              	<FlatList
	                	data={this.state.items}
	                	keyExtractor={(item) =>item.id.toString()}
	                	renderItem={({item}) =><PanierItem item={item} setQuantity = {this._setQuantity}/>}
	                	style ={styles.liste}/>
	            </View>
              	<View style={styles.footer_container}>
					<Text style={styles.total}>Total: {this.state.total} €</Text>
					<TouchableOpacity style={styles.livrer} onPress={()=>{this._switchModalVisibility()}}>
						<Text>Lieu de livraison (choisir):</Text>
						<Text>   {this.state.livraison.name}{"\n"}   {this.state.livraison.adresse}</Text>
					</TouchableOpacity>
					<View style={{flexDirection: "row"}}>
						<View style={{ width : "70%"}}>
							<Text>Date de livraison :</Text>
							<Text>   Le {this._dateLivraison()}, à partir de 9h.</Text>
						</View>
						<View style={{ width : "100%"}}>
							<TouchableOpacity disabled={this.state.total>0?false:true} style={styles.valider} onPress={()=> {this._switchVisibility()}}>
								<Text style={styles.textValider}>Valider</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
		    </View>
			</ImageBackground>  
		 )
	}
}

const styles = StyleSheet.create({
  main_container: {
	flex: 1,
	height: 2,
  },  header_container: {
	flex: 0.15,
	height: 2
  },  items_container: {
	flex: 0.6,
	height: 2,
	borderColor : "black",
	borderWidth : 1
  },  footer_container: {
	flex: 0.25,
	height: 2
  },
  text:{
	fontWeight: 'bold',
    fontSize: 30,
    flex: 1,
    flexWrap: 'wrap',
	paddingRight: 5,
	textAlign: 'center'
  },
  valider :{
  	//flex : 1,
  	//backgroundColor:'blue',
	borderColor : "black",
	borderWidth : 1,
  	width : "25%"
  },
  textValider :{
  	fontSize : 18,
  	textAlign:"center",
  	textAlignVertical:"center"
  },
  
image: {
	width: 340,
	height: 400,
	margin: 10,
	backgroundColor: 'gray',
	flexWrap: 'wrap',
	alignItems: 'center',
	justifyContent: "center",
},
  background: {
    width: '100%',
    height: '100%',
    flex: 1,
    //resizeMode: "cover",
    //justifyContent: "center"
  },
  total : {
  	fontSize : 18,
  	textAlign : "right"
  }, modal : {
  	margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  }

})

export default Panier
