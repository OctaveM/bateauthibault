// Components/ProduitItem.js

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList , Button,Image, TouchableOpacity } from 'react-native'
import Icons from './Icons.js'

class ProduitItem extends React.Component {
  render() {
    const { produit, basketAdd }  = this.props
  
    return (
      <TouchableOpacity   style={styles.main_container} disabled={produit.availability?false:true} onPress={() =>{basketAdd(produit)}}>
        <Image style={styles.image} source = {require('../assets/poulpe.png')} />
       <Text style={styles.text}>{produit.name}</Text>
       <Text style={styles.prix}>{produit.availability? produit.price+"€":"Non Disp."}</Text>
      </TouchableOpacity>

    )
  }
}

const styles = StyleSheet.create({
  main_container: {
    height: 45,
    margin : 5,
    flexDirection: 'row',
    backgroundColor: 'rgba(120, 120, 120, 0.8)'
    //opacity : 0.5
  },

  image: {
    width: 40,
    height: 40,
    marginTop : 2.5,
    marginLeft: 10,
    marginRight: 10
  },
  content_container: {
    flex: 10,
    margin: 5
  },
  header_container: {
    flex: 3,
    flexDirection: 'row'
  },text: {
    opacity: 1,
    textAlignVertical : 'center'
  },
  nom: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  },
  prix: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5,
    textAlignVertical : 'center',
    textAlign:'right'
  }
})


export default ProduitItem
