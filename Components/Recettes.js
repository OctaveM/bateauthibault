// Components/Recettes.js

//Vue4.png

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList } from 'react-native'
import recette from '../assets/StaticData/recetteData'
import RecetteItem from './RecetteItem'

class Recettes extends React.Component {
     _displayDetailForRecette = ( nameRecette,uriRecette, desc) => {
        this.props.navigation.navigate("Recette",{nameRecette:nameRecette, uriRecette: uriRecette ,desc:desc} )
      }
      render() {
      
          return (
              <View style={styles.container}>
                <ImageBackground source={require('../assets/splash.png')} style={styles.image}>
                <Text style={styles.text}>Recettes </Text>
                <FlatList
                  data={recette}
                  keyExtractor={(item) =>item.id.toString()}
                  renderItem={({item}) =><RecetteItem recette={item} displayDetailForRecette = {this._displayDetailForRecette}/>}
                  style ={styles.liste}
                />
              </ImageBackground>  
            </View>
  
          )
  
      }

}


const styles = StyleSheet.create({
    container: {
      flex: 1
      //backgroundColor: '#fff',
      //alignItems: 'center',
      //justifyContent: 'center',
    },
    image: {
      width: '100%',
      height: '100%',
      flex: 1,
      //resizeMode: "cover",
      //justifyContent: "center"
    },
    text:{
      fontWeight: 'bold',
        fontSize: 30,
        flex: 0.1,
        flexWrap: 'wrap',
      paddingRight: 5,
      textAlign: 'center'
      },
     liste:{
      flex: 1
    }
  });

export default Recettes
