// Components/Bateaux.js

//Vue2.png

import React from 'react'
import { Text, ImageBackground, StyleSheet, View, FlatList } from 'react-native'
import bateaux from '../assets/StaticData/bateauData.js'
import BateauItem from './BateauItem'


class Bateaux extends React.Component {
    _displayDetailForBateau = ( nameBateau,uriBateau,desc) => {
	  this.props.navigation.navigate("Bateau",{ nameBateau:nameBateau, uriBateau: uriBateau, desc: desc } )
    }
    render() {
	
        return (
  	      <View style={styles.container}>
	          <ImageBackground source={require('../assets/splash.png')} style={styles.image}>
              <Text style={styles.text}>La liste des Bateaux </Text>
              <FlatList
                data={bateaux}
                keyExtractor={(item) =>item.id.toString()}
                renderItem={({item}) =><BateauItem bateau={item} displayDetailForBateau = {this._displayDetailForBateau}/>}
                style ={styles.liste}
              />
            </ImageBackground>  
          </View>

        )

    }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
    //backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    flex: 1,
    //resizeMode: "cover",
    //justifyContent: "center"
  },
  text:{
    fontWeight: 'bold',
      fontSize: 25,
      flex: 0.1,
      flexWrap: 'wrap',
    paddingRight: 5,
    textAlign: 'center'
    },
  liste:{
      flex: 1
    }
});

export default Bateaux
