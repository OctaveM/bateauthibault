// Components/Accueil.js

//Vue0.png

import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity ,Image} from 'react-native'

class Accueil extends React.Component {

    render() {

        return (

            <View>
				<View style ={{marginTop : 40}}>
					<Text style={styles.app_name}>Le bateau de Thibault</Text>
					<Text style={styles.info}>Bienvenue</Text>
				</View>
				<View style = {styles.view}>
					<TouchableOpacity style={styles.main_container} onPress={()=> {this.props.navigation.navigate("SelectionTypeProduit")}}>
						<View style={styles.container_button}>
							<Image source={require('../assets/Images/Accueil/poisson.png')} style={styles.image}/>
							<Text style = {styles.title_text}>Produits et promotions</Text>
						</View>
					</TouchableOpacity>
					<View style = {styles.container_button}>
						<TouchableOpacity style = {styles.position_button} onPress={()=> {this.props.navigation.navigate("Bateaux")}}>
							<View style={styles.container_button}>
								<Image source={require('../assets/Images/Accueil/ancre.png')} style={styles.image}/>
								<Text style = {styles.title_text}>Bâteaux</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity style = {styles.position_button} onPress={()=> {this.props.navigation.navigate("Restaurants")}}>
							<View style={styles.container_button}>
								<Image source={require('../assets/Images/Accueil/restaurant.png')} style={styles.image}/>
								<Text style = {styles.title_text}>Restaurants</Text>
							</View>
							
						</TouchableOpacity>
					</View>
					<View style = {styles.container_button}>
						<TouchableOpacity style = {styles.position_button} onPress={()=> {this.props.navigation.navigate("Recettes")}}>
							<View style={styles.container_button}>
								<Image source={require('../assets/Images/Accueil/recette.png')} style={styles.image}/>
								<Text style = {styles.title_text}>Recettes</Text>
							</View>
							
						</TouchableOpacity>
						<TouchableOpacity style = {styles.position_button} onPress={()=> {this.props.navigation.navigate("Gerant")}}>
							<View style={styles.container_button}>
								<Image source={require('../assets/Images/Accueil/tourteau.png')} style={styles.image2}/>
								<Text style = {styles.title_text}>Contact</Text>
							</View>
							
						</TouchableOpacity>
					</View>
					
				</View>
            </View>

        )

    }

}

const styles = StyleSheet.create({
	app_name :{
		fontSize:30,
		fontWeight: 'bold',
		textAlign: 'center',
		justifyContent: 'center',
		
	
	},
	info : {
		fontSize:25,
		fontWeight: 'bold',
		textAlign: 'center',
    	justifyContent: 'center',
	},
	main_container: {
		height: 70,
		margin : 0.5,
		flexDirection: 'column',
		backgroundColor: 'rgba(120, 120, 120, 0.8)',
		borderColor : "black",
		borderWidth : 1,
	  },
	   position_button :  {
		height: 90,
		margin : 0.5,
		flexDirection: 'column',
		backgroundColor: 'rgba(120, 120, 120, 0.8)',
		borderColor : "black",
		borderWidth : 1,
		width:'50%'
	  },
	 
	  view :{
		marginTop:100,
	  },
	  container_button:{
		position: "relative",
		flexDirection: "row",
		justifyContent: "center",

	},
	image: {
		width: 50,
		height: 50,
		margin: 10,
		backgroundColor: 'gray',
		borderRadius: 50/ 2
	},

	image2: {
		width: 50,
		height: 50,
		margin:10,
		backgroundColor: 'gray',
		borderRadius: 50/ 2,
		position: "relative",

	},
	title_text: {
	fontWeight: 'bold',
	fontSize: 18,
	flex: 1,
	flexWrap: 'wrap',
	paddingRight: 5,
	textAlign: 'center',
	justifyContent: 'center',
	textAlignVertical : 'center'
	}
})
export default Accueil
