# Application Bateau Thibault

Auteurs : Nayani George Clement et Octave Monvoisin

# Utilisation
Notre application utilise Expo, pour la lancer faite
`
npm start
`

Une page web s'ouvrira dans votre navigateur. Pour tester l'application, vous pourrez alors soit lancer un simulateur IOS ou Android préalablement installé sur votre ordinateur et sélectionner respectivement "Run on iOS simulator" ou "Run on Android device/emulator" ou vous pouvez télécharger l'application pour [iOS](https://apps.apple.com/app/apple-store/id982107779) ou [Android](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www) et scannez le QR code en bas à gauche de la page web. 
