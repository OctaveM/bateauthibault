// Navigation/Navigation.js

import {createAppContainer} from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import Accueil from '../Components/Accueil'
import Bateaux from '../Components/Bateaux'
import Restaurants from '../Components/Restaurants'
import Recettes from '../Components/Recettes'

import Bateau from '../Components/Bateau'
import Gerant from '../Components/Gerant'
import Recette from '../Components/Recette'
import Restaurant from '../Components/Restaurant'


import SelectionTypeProduit from '../Components/SelectionTypeProduit'
import Produits from '../Components/Produits'


import Panier from '../Components/Panier'

const AccueilStackNavigator = createStackNavigator({
  Accueil: { 
    screen: Accueil,
    navigationOptions: {
      title: 'Accueil',
       headerShown: false
    }
  },
  Bateaux : {
    screen: Bateaux,
    navigationOptions: {
      title: 'Bateaux',
       headerShown: false
    }
  },
  Restaurants : {
    screen: Restaurants,
    navigationOptions: {
      title: 'Restaurants',
       headerShown: false
    }
  },
  Recettes : {
    screen: Recettes,
    navigationOptions: {
      title: 'Recettes',
       headerShown: false
    }
  }, 
  Gerant : {
  	screen: Gerant,
  	navigationOptions: {
      title: 'Gerant',
       headerShown: false
    }
  },
  
  Bateau : {
    screen:Bateau,
    navigationOptions: {
      title: 'Bateau',
       headerShown: false
    }
  },

  Recette : {
    screen:Recette,
    navigationOptions: {
      title: 'Recette',
       headerShown: false
    }
  },
  Restaurant : {
    screen: Restaurant,
    navigationOptions: {
      title: 'Restaurant',
       headerShown: false
    }
  },
  SelectionTypeProduit : {
    screen: SelectionTypeProduit,
    navigationOptions: {
      title: 'Selection type de Produit',
       headerShown: false
    }
  },
  Produits : {
    screen: Produits,
    navigationOptions: {
      title: 'Produits',
       headerShown: false
    }
  },
  Panier : {
    screen: Panier,
    navigationOptions: {
      title: 'Panier',
       headerShown: false
    }
  }
 
}, { //defaultNavigationOptions : {
	mode: "card",
	cardOverlayEnabled: true,
	transparentCard: true,
	//cardstyle: { backgroundColor: 'transperent'}	
//}
}
);


export default createAppContainer(AccueilStackNavigator)
