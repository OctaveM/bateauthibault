import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Navigation from './Navigation/Navigation';
import { StyleSheet, View, ImageBackground } from 'react-native';

import Accueil from './Components/Accueil'



export default function App() {
  console.disableYellowBox = true;
  return (
  <View style={styles.container}>
    <ImageBackground source={require('./assets/splash.png')} style={styles.image}>
      <Navigation/>
    </ImageBackground>
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 25,
    //backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
    flex: 1,
    //resizeMode: "cover",
    //justifyContent: "center"
  },
});
