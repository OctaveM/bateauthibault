// Helpers/filmsData.js

export default data = [
    {
        id: 0,
        name: "Poissons",
        var_check: "category",
        value_check: 0
    },
    {
        id: 1,
        name: "Coquillages",
        var_check: "category",
        value_check: 1
    },
    {
        id: 2,
        name: "Crustacés",
        var_check: "category",
        value_check: 2
    },
    {
        id: 3,
        name: "Promotions",
        var_check: "sale",
        value_check: true
    }]
