// Helpers/filmsData.js

export default data = [

    {
        id: 0,
        name: "Bistrot Landias",
        uri: "bistrotLandais",
        icon: "bistrotLandais_icon",
        adresse: "26 Avenue de Tourville, 75007 Paris",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    },
    {
        id: 1,
        name: "Bistrot des Gascons",
        uri: "desGascons",
        icon: "desGascons_icon",
        adresse: "27 Avenue de Tourville, 75007 Paris",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    },
  
    {
        id: 2,
        name: "Bistrot du Sommelier",
        uri: "duSommelier",
        icon: "duSommelier_icon",
        adresse: "26 Avenue de Tourville, 75007 Paris",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    },
    {
        id: 3,
        name: "Les fous de l'île",
        uri: "fousDeLIle",
        icon: "fousDeLIle_icon",
        adresse: "26 Avenue de Tourville, 75007 Paris",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    },
    {
        id: 4,
        name: "Villa 9-Trois",
        uri: "villa9Trois",
        icon: "villa9Trois_icon",
        adresse: "26 Avenue de Tourville, 75007 Paris",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    }
   
]
