// Helpers/filmsData.js

export default data = [
    {
        id: 0,
        name: "Bar Recette",
        uri: "barRecette",
        icon: "barRecette_icon",
        desc: "Sur une plaque ou un plat allant au four, disposer  quelques feuilles de laurier frais, verser un filet d'huile d'olive et du gros sel. Disposer le bar, puis l'arroser d'un filet d'huile d'olive et mettre un peu de gros sel sur la peau.Cuire au four pendant 12 min."
    },
    {
        id: 1,
        name: "Homard Recette",
        uri: "homardRecette",
        icon: "homardRecette_icon",
        desc: "Faites cuire les homards dans de l'eau bouillante avec du thym, du laurier, du sel et du poivre de Cayenne. Laissez cuire 20 minutes.Gouttez-les et laissez-les refroidir.coupez les coffres des homards dans le sens de la longueur.langez la mayonnaise avec le cognac, le corail et la ciboulette cisele."
    },
    {
        id: 2,
        name: "Saint-Jacques",
        uri: "saintJacques",
        icon: "saintJacques_icon",
        desc: "Faire fondre du beurre avec des chalotes puis ajouter les noix de Saint-Jacques. Les faire revenir en laissant le milieu translucide puis les retirer du feu Ajouter l'ail et le persil dans le et laisser cuire quelques secondes. Bien faire chauffer la po&#xea;le, puis flamber au Cognac. Une fois la flamme teinte, ajouter les noix de Saint-Jacques (il ne faut pas les flamber car elles perdraient leur saveur).guster chaud nature ou accompagne d'une fondue de poireaux."
    }]
