// Helpers/filmsData.js

export default data = [
    {
        id: 0,
        name: "Aquilon",
        uri: "aquilon",
        icon: "aquilon_icon",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    },
    {
        id: 1,
        name: "De la brise",
        uri: "deLaBrise",
        icon: "deLaBrise_icon",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    },
    {
        id: 2,
        name: "Saphir",
        uri: "saphir",
        icon: "saphir_icon",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    },
    {
        id: 3,
        name: "Gast Micher",
        uri: "gastMicher",
        icon: "gastMicher_icon",
        desc: "Qu'il est chaud le soleil Quand nous sommes en vacances Y a d' la joie, des hirondelles C'est le sud de la France Papa bricole au garage Maman lit dans la chaise longue Dans ce joli paysage Moi, je me balade en tongs Que de bonheur! Que de bonheur!"
    }]
